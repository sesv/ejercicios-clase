

public class Adivinanza {
	public static void main (String[] args){
		java.util.Scanner lectura = new java.util.Scanner(System.in);
		String playAgain="y";
		while (playAgain=="y"){
			GuessingGame();
			System.out.println("Would you like to play another game (y/n):");
			playAgain=lectura.next();
		}
	}
	public static void GuessingGame() {
		java.util.Scanner lectura = new java.util.Scanner(System.in);
		int answer = (int) Math.floor((Math.random()*100)+1);
		int guess=0;
		int numGuesses=0;
		while ((numGuesses<7) && (guess!=answer)){
			System.out.println("Guess a number: ");
			guess=lectura.nextInt();
			if (guess<answer){
				System.out.println("Higher...");
			}else{
				if (guess>answer){
					System.out.println("Lower...");
				}else{
					System.out.println("You win");
				}
			}
			numGuesses=numGuesses+1;
		}
		if (numGuesses>=7){
			System.out.println("You loser");
			System.out.println("The answer was: "+answer);
		}
	}
}
