
import java.util.Arrays;

public class LocalComercial {

	public static void main(String[] args) {
		java.util.Scanner input= new java.util.Scanner(System.in);
		boolean primerIngreso=true;
		String[][] datosProductos=new String[20][3];	
		int[][] datosVentas=new int[50][10];
		String[][] datosVendedores=new String[20][3];
		int[] cProm=null;
		boolean continuar=true;
		while (continuar){
			if (primerIngreso){
				/*System.out.println("Por favor ingrese los datos de sus productos, sus vendedores y sus ventas.");
				datosProductos=DatosProductos(datosProductos,primerIngreso);
				datosVendedores=DatosVendedores(datosVendedores,primerIngreso);
				datosVentas=DatosVentas(datosVentas,primerIngreso);
				cProm=CodigosPromocionales();*/
				datosProductos=AutoFillProductos(datosProductos);
				datosVendedores=AutoFillVendedores(datosVendedores);
				datosVentas=AutoFillVentas(datosVentas);
				primerIngreso=false;
			}else{
				System.out.println("¿Qué operación desea realizar?");
				System.out.println(" 1. Agregar productos.");
				System.out.println(" 2. Agregar vendedores.");
				System.out.println(" 3. Agregar venta.");
				System.out.println(" 4. Ver vendedor con más ventas.");
				System.out.println(" 5. Ver ventas con tarjeta debito");
				System.out.println(" 6. Buscar una venta.");
				System.out.println(" 7. Ver el total de ingresos de las ventas.");
				System.out.println(" 8. Generar factura de cada venta.");
				System.out.println(" 9. Ver la venta de mayor importe realizada con tarjeta.");
				System.out.println(" 10. Finalizar sesión.");
			int eleccion = input.nextInt();
			switch(eleccion){
				case 1:
					datosProductos=DatosProductos(datosProductos,primerIngreso);
					break;
				case 2:
					datosVendedores=DatosVendedores(datosVendedores,primerIngreso);
					break;
				case 3:
					datosVentas=DatosVentas(datosVentas,primerIngreso);
					break;
				case 4:
					VendedorEstrella(datosVentas,datosVendedores);
					break;
				case 5:
					VentasDebito(datosVentas);
					break;
				case 6:
					BuscadorVentas(datosVentas, datosVendedores, datosProductos);
					break;
				case 7:
					IngresosTotales(datosVentas,datosProductos);
					break;
				case 8:
					Facturas(datosVentas, datosVendedores, datosProductos);
					break;
				case 9:
					VentaCredito(datosVentas);
					break;
				case 10:
					continuar=false;
					break;
				default:
					System.out.println("Ingrese una opción válida.\n\n");	
			}		
			}
		}
	}
	
	public static String[][] DatosProductos(String[][] datosProductos, boolean primerIngreso){
		java.util.Scanner input= new java.util.Scanner(System.in);
		while (true){
			System.out.println("Ingrese el n�mero de productos que va a ingresar");
			int cantP=input.nextInt();
			if(primerIngreso){
				if(cantP<=20){
					datosProductos=new String[20][3];
					for (int i=0; i<cantP;i++){
						System.out.println("Ingrese el codigo del producto "+(i+1));
						datosProductos[i][0]=input.next();
						System.out.println("Ingrese el nombre del producto "+(i+1));
						datosProductos[i][1]=input.next();
						System.out.println("Ingrese el precio unitario del producto "+(i+1));
						datosProductos[i][2]=input.next();
					}
					break;
				}else{
					System.out.println("No hay espacio en memoria, ingrese un valor v�lido. Puede ingresar hasta 20 productos.");
				}
			}else{
				int x=0;
				for (int i=0;i<20;i++){
					if(datosProductos[i][0]==null){
						x=i;
						break;
					}
				}
				if((x+cantP)<=20){
					for(int k=x;k<(cantP+x);k++){
						System.out.println("Ingrese el codigo del producto "+(k+1));
						datosProductos[k][0]=input.next();
						System.out.println("Ingrese el nombre del producto "+(k+1));
						datosProductos[k][1]=input.next();
						System.out.println("Ingrese el precio unitario del producto "+(k+1));
						datosProductos[k][2]=input.next();
					}
					break;
				}else{
					System.out.println("No hay espacio en memoria, ingrese un valor v�lido. Puede ingresar  "
					+(20-x)+" productos m�s.");
				}
			}
		}	
		return datosProductos;
	}
	
	public static int[][] DatosVentas(int[][] datosVentas, boolean primerIngreso){
		java.util.Scanner input= new java.util.Scanner(System.in);
		while(true){
			System.out.println("Ingrese el n�mero ventas a ingresar.");
			int cantV=input.nextInt();
			if(primerIngreso){
				if (cantV<=50){
					datosVentas= new int [50][10];
					for (int i=0; i<cantV;i++){
						System.out.println("Ingrese el d�a de la venta "+(i+1));
						datosVentas[i][0]=input.nextInt();
						System.out.println("Ingrese el mes de la venta "+(i+1));
						datosVentas[i][1]=input.nextInt();
						System.out.println("Ingrese el a�o de la venta "+(i+1));
						datosVentas[i][2]=input.nextInt();
						System.out.println("Ingrese la hora de la venta (s�lo en horas) "+(i+1));
						datosVentas[i][3]=input.nextInt();
						System.out.println("Ingrese los minutos de la hora de la venta "+(i+1));
						datosVentas[i][4]=input.nextInt();
						System.out.println("Ingrese el ID del vendedor "+(i+1));
						datosVentas[i][5]=input.nextInt();
						System.out.println("Ingrese el codigo del producto vendido "+(i+1));
						datosVentas[i][6]=input.nextInt();
						System.out.println("Ingrese el precio unitario del producto "+(i+1));
						datosVentas[i][7]=input.nextInt();
						System.out.println("Ingrese la cantidad que se vendi� "+(i+1));
						datosVentas[i][8]=input.nextInt();
						System.out.println("Ingrese la forma de pago (0 - efectivo, 1 - debito, 2 - tarjeta) "+(i+1));
						datosVentas[i][9]=input.nextInt();
					}
					break;
				}else{
					System.out.println("No hay espacio en memoria, ingrese un valor v�lido. Puede ingresar hasta 20 productos.");
				}
			}else{
				int x=0;
				for (int i=0;i<50;i++){
					if(datosVentas[i][0]==0){
						x=i;
						break;
					}
				}
				if((cantV+x)<=50){
					for (int k=x;k<=(cantV+x);k++){
						System.out.println("Ingrese el d�a de la venta "+(k+1));
						datosVentas[k][0]=input.nextInt();
						System.out.println("Ingrese el mes de la venta "+(k+1));
						datosVentas[k][1]=input.nextInt();
						System.out.println("Ingrese el a�o de la venta "+(k+1));
						datosVentas[k][2]=input.nextInt();
						System.out.println("Ingrese la hora de la venta (s�lo en horas) "+(k+1));
						datosVentas[k][3]=input.nextInt();
						System.out.println("Ingrese los minutos de la hora de la venta "+(k+1));
						datosVentas[k][4]=input.nextInt();
						System.out.println("Ingrese el ID del vendedor "+(k+1));
						datosVentas[k][5]=input.nextInt();
						System.out.println("Ingrese el codigo del producto vendido"+(k+1));
						datosVentas[k][6]=input.nextInt();
						System.out.println("Ingrese el precio unitario del producto "+(k+1));
						datosVentas[k][7]=input.nextInt();
						System.out.println("Ingrese la cantidad que se vendi� "+(k+1));
						datosVentas[k][8]=input.nextInt();
						System.out.println("Ingrese la forma de pago (0 - efectivo, 1 - debito, 2 - tarjeta) "+(k+1));
						datosVentas[k][9]=input.nextInt();					}
				}else{
					System.out.println("No hay espacio en memoria, ingrese un valor v�lido. Puede ingresar  "
					+(50-x)+" productos m�s.");
				}
			}
		}
		return datosVentas;
	}
	
	public static String[][] DatosVendedores(String[][]datosVendedores,boolean primerIngreso){
		java.util.Scanner lectura=new java.util.Scanner(System.in);
		System.out.println("Ingrese el n�mero de vendedores:");
		int numV=lectura.nextInt();
		while(true){
			if (primerIngreso){
				if (numV<=20){
					datosVendedores=new String [20][3];
					for (int i=0;i<numV;i++){
						System.out.println("Ingrese el ID del vendedor "+(i+1)+":");
						datosVendedores[i][0]=lectura.next();
						System.out.println("Ingrese el Nombre del vendedor "+(i+1)+":");
						datosVendedores[i][1]=lectura.next();
						System.out.println("Ingrese el Apellido del vendedor "+(i+1)+":");
						datosVendedores[i][2]=lectura.next();
					}
					break;
				}else{
					System.out.println("No hay espacio en memoria, ingrese un valor v�lido. Puede ingresar hasta 20 productos.");
				}
			}else{
				int x=0;
				for (int i=0;i<20;i++){
					if (datosVendedores[i][0]==null){
						x=i;
						break;
					}
				}
				if((numV+x)<=20){
					for (int i=x;i<(numV+x);i++){
						System.out.println("Ingrese el ID del vendedor "+(i+1)+":");
						datosVendedores[i][0]=lectura.next();
						System.out.println("Ingrese el Nombre del vendedor "+(i+1)+":");
						datosVendedores[i][1]=lectura.next();
						System.out.println("Ingrese el Apellido del vendedor "+(i+1)+":");
						datosVendedores[i][2]=lectura.next();
					}
				break;
				}else{
					System.out.println("No hay espacio en memoria, ingrese un valor v�lido. Puede ingresar "+(20-x)+" productos m�s.");
				}
			}
		}
		return datosVendedores;
	}
	
	public static int[] CodigosPromocionales(){
		java.util.Scanner lectura=new java.util.Scanner(System.in);
		System.out.println("Ingrese el n�mero de c�digos promocionales:");
		int numC=lectura.nextInt();
		int cProm[]=new int [numC];
		for (int i=0;i<numC;i++){
			cProm[i]=(int) Math.floor((Math.random()*(1000-9999)+9999));
		}
		return cProm;
	}
	
	public static void VendedorEstrella(int[][] datosVentas,String[][] datosVendedores){
		int x=0;
		for (int i=0;i<20;i++){
			if (datosVendedores[i][0]==null){
				x=i;
				break;
			}
		}
		int y=0;
		for (int i=0;i<50;i++){
			if (datosVentas[i][0]==0){
				y=i;
				break;
			}
		}
		int c=0;
		int d=0;
		int b=0;
		int sum=0;
		for(int j=0;j<x;j++){
			for(int i=0;i<y;i++){
				b=Integer.parseInt(datosVendedores[j][0]);
				if (datosVentas[i][5]==b){
					sum+=datosVentas[i][8];
				}
			}
			if (sum>c){
				c=sum;
				d=j;
			}
			sum=0;
		}
		System.out.println("El vendedor con más venta tiene la siguiente información:");
		System.out.println("El ID del vendedor es: "+datosVendedores[d][0]);
		System.out.println("El nombre del vendedor es: "+datosVendedores[d][1]);
		System.out.println("El apellido del vendedor es: "+datosVendedores[d][2]);
	}
	
	public static void VentasDebito(int[][] datosVentas){	
		int x=0;
		for (int i=0;i<50;i++){
			if(datosVentas[i][0]==0){
				x=i;
				break;
			}
		}
		int[] ventasDebitoID= new int[x];
		for(int i=0;i<x;i++){
			for(int k=0;k<x;k++){
				if(datosVentas[i][8]==1){
					ventasDebitoID[k]=datosVentas[i][8];
				}
			}
		}				
		int[] tmp=ventasDebitoID.clone();
		Arrays.sort(tmp);
		int[] subtotal=new int[x];
		for (int j=0;j<x;j++){
			for (int h=0;h<x;h++){
				if(tmp[j]==ventasDebitoID[h]){
					for(int g=0;g<x;g++){
						if((datosVentas[h][7]*datosVentas[h][8])!=subtotal[g]){
								subtotal[g]=datosVentas[h][7]*datosVentas[h][8];
								System.out.println(ventasDebitoID[h]+"\t"+"$"+subtotal[g]);
							break;
						}else{
							if(g==(x-1)){
								System.out.println(ventasDebitoID[h]+"\t"+"$"+datosVentas[h][7]*datosVentas[h][8]);
							}
						}
					}
				}
			}
		}
	}

	public static void BuscadorVentas(int[][] datosVentas, String[][] datosVendedores, String[][]datosProductos){
		java.util.Scanner input=new java.util.Scanner(System.in);
		int x=0;
		for (int i=0;i<20;i++){
			if(datosVendedores[i][0]==null){
				x=i;
				break;
			}
		}
		int[] vendedoresID=new int[(x+1)];
		for (int k=0;k<vendedoresID.length;k++){
			vendedoresID[k]=Integer.parseInt(datosVendedores[k][0]);
		}
		int[] productosCodigo=new int[x];
		for (int k=0;k<productosCodigo.length;k++){
			productosCodigo[k]=Integer.parseInt(datosProductos[k][0]);
		}
		boolean continuar=true;
		while (continuar){
			for (int j=0;j<(x+1);j++){
				System.out.println("\t"+datosProductos[j][0]+"\t"+datosProductos[j][1]+"\t"+datosProductos[j][2]);
			}
			System.out.println("Ingrese el codigo del vendedor que desea consultar.");
			int eleccionProducto=input.nextInt();
			for (int i=0;i<20;i++){
				if(datosProductos[i][0]==null){
					x=i;
					break;
				}
			}
			for (int j=0;j<(x+1);j++){
				System.out.println("\t"+datosVendedores[j][0]+"\t"+datosVendedores[j][1]+"\t"+datosVendedores[j][2]);
			}
			System.out.println("Ingrese el codigo del vendedor que desea consultar.");
			int eleccionVendedor=input.nextInt();
			int y=0;
			for (int i=0;i<20;i++){
				if(datosVendedores[i][0]==null){
					y=i;
					break;
				}
			}
			int cantVnts=0;
			for (int i=0;i<50;i++){
				if(datosVentas[i][0]==0){
					cantVnts=i+1;
					break;
				}
			}
			for(int i=0;i<cantVnts;i++){
				for(int k=0;k<vendedoresID.length;k++){
					if(eleccionVendedor==vendedoresID[k]){
						x=k;
						break;
					}
				}
				for(int j=0;j<productosCodigo.length;j++){
					if(eleccionProducto==productosCodigo[j]){
						y=j;
						break;
					}
				}
				if(y==x){
					System.out.println("Los resultados de su b�squeda son:");
					System.out.println("\t Horas \t Minutos \t D�a \t Mes \t A�o \t ID "+
						"\t vendedor \t Codigo producto \t Cantidad \t Precio unitario \t Total \t Forma de pago");
					System.out.println("\t "+datosVentas[x][3]+" \t "+datosVentas[x][4]+ " \t "+datosVentas[x][0]+
						" \t "+datosVentas[x][1]+" \t "+datosVentas[x][2]+" \t "+datosVentas[x][5]+" \t "+datosVentas[x][6]+
						" \t "+datosVentas[x][8]+" \t "+datosVentas[x][7]+" \t "+
						(datosVentas[x][7]*datosVentas[x][8])+" \t "+datosVentas[x][9]);
					
				}else{
					if(i==(cantVnts-1)){
						System.out.println("No se encontraron resultados");
					}
				}
			}
		System.out.println("�Desea hacer otra b�squeda? (1 - Si, 0 - No)");
		int yn=input.nextInt();
		if(yn==0){
			continuar=false;
		}
		}
	}
	
	public static void Facturas(int[][] datosVentas, String[][] datosVendedores, String[][] datosProductos){
		java.util.Scanner input=new java.util.Scanner(System.in);
		int cantVnts=0;
		int cantP=0;
		int cantV=0;
		for (int i=0;i<50;i++){
			if(datosVentas[i][0]==0){
				cantVnts=i;
				break;
			}
		}
		
		for (int i=0;i<20;i++){
			if(datosVendedores[i][0]==null){
				cantV=i;
				break;
			}
		}
		int[] vendedoresID=new int[(cantV+1)];
		for (int k=0;k<(vendedoresID.length-1);k++){
			vendedoresID[k]=Integer.parseInt(datosVendedores[k][0]);
		}
		for (int i=0;i<20;i++){
			if(datosProductos[i][0]==null){
				cantP=i;
				break;
			}
		}
		int[] productosCodigo=new int[cantP+1];
		for (int k=0;k<(productosCodigo.length-1);k++){
			productosCodigo[k]=Integer.parseInt(datosProductos[k][0]);
		}
		for(int i=0;i<cantVnts;i++){
			System.out.println("**************************************************************************"
					+ "***************************\n");
			System.out.println("Fecha y hora de la venta: "+datosVentas[i][0]+"/"+datosVentas[i][1]+"/"+datosVentas[i][2]+
				"\t"+datosVentas[i][3]+":"+datosVentas[i][4]+"\n");
			System.out.println("Datos del vendedor: ");
			System.out.println("\t ID \t Nombre \t Apellido");
			//relacionar arreglo ID para imprimir nobre y apellido
			for(int k=0;k<vendedoresID.length;k++){
				if(datosVentas[i][5]==vendedoresID[k]){
					System.out.println("\t "+datosVendedores[k][0]+"\t "+datosVendedores[k][1]+"\t\t "+datosVendedores[k][2]);
				}
			}
			System.out.println("Datos de la venta: ");
			System.out.println("\t Codigo    Nombre       Precio unitario     Cantidad     Subtotal venta     Forma de pago");
			//relacionar arreglo codigo para imprimir datos de la venta
			for (int h=0;h<productosCodigo.length;h++){
				if(datosVentas[i][6]==productosCodigo[h]){
					System.out.print(" \t "+datosVentas[i][6]+" \t   "+datosProductos[h][1]+" \t "+datosVentas[i][7]+
							" \t\t\t"+datosVentas[i][8]+"\t\t"+(datosVentas[i][7]*datosVentas[i][8])+" \t\t ");
					switch(datosVentas[i][9]){
					case 0:
						System.out.println("Efectivo");
						break;
					case 1:
						System.out.println("Debito");
						break;
					case 2:
						System.out.println("Tarjeta");
						break;
				}
				}
			}
			System.out.println("**************************************************************************"
					+ "***************************\n");
		}	
	}

	public static void IngresosTotales(int[][]datosVentas,String[][]datosProductos ){
		int x=0;
		for (int i=0;i<50;i++){
			if (datosVentas[i][0]==0){
				x=i;
				break;
			}
		}
		int y=0;
		for (int i=0;i<20;i++){
			if (datosProductos[i][0]==null){
				y=i;
				break;
			}
		}
		int b=0;
		int a=0;
		int c=0;
		int d=0;
		for (int i=0;i<y;i++){
			for (int j=0;j<x;j++){
				b=Integer.parseInt(datosProductos[i][0]);
				if (datosVentas[j][6]==b){
					a+=datosVentas[j][8];
				}
			}
			c=Integer.parseInt(datosProductos[i][2]);
			d+=(c*a);
			a=0;
		}
		System.out.println("El monto total de ventas realizadas es: $"+d);
	}
	
	public static void VentaCredito(int[][]datosVentas){
		int x=0;
		for (int i=0;i<50;i++){
			if (datosVentas[i][0]==0){
				x=i;
				break;
			}
		}
		int b;
		int sum=0;
		int c=0;
		int d=0;
		for (int i=0;i<x;i++){
			if (datosVentas[i][9]==2){
				sum+=datosVentas[i][7]*datosVentas[i][8];
			}
			if (sum>c){
				c=sum;
				d=i;
			}
			sum=0;
		}
		System.out.println("\t Fecha \t\t Hora \t ID vendedor \t Codigo producto \t Precio unitario \t Cantidad \t Total");
		System.out.println("\t"+datosVentas[d][0]+"/"+datosVentas[d][1]+"/"+datosVentas[d][2]+" \t "+
				datosVentas[d][3]+":"+datosVentas[d][4]+" \t "+datosVentas[d][5]+" \t \t "+datosVentas[d][6]+" \t \t \t "+
				datosVentas[d][7]+" \t \t \t "+datosVentas[d][8]+" \t \t "+(datosVentas[d][7]*datosVentas[d][8]));
	}

	public static String[][] AutoFillVendedores(String[][] datosVendedores){
		datosVendedores[0][0]="1024";datosVendedores[0][1]="Luis";datosVendedores[0][2]="Rodriguez";
		datosVendedores[1][0]="1001";datosVendedores[1][1]="Sergio";datosVendedores[1][2]="Sanchez";
		return datosVendedores;
	}
	
	public static String[][] AutoFillProductos(String[][] datosProductos){
		datosProductos[0][0]="4056";
		datosProductos[0][1]="Sandwich";
		datosProductos[0][2]="3000";
		
		datosProductos[1][0]="3045";
		datosProductos[1][1]="Jugo";
		datosProductos[1][2]="2000";
		return datosProductos;
	}
	
	public static int[][] AutoFillVentas(int[][] datosVentas){
		datosVentas[0][0]=2;datosVentas[0][1]=1;datosVentas[0][2]=2015;datosVentas[0][3]=14;datosVentas[0][4]=24;
		datosVentas[0][5]=1024;datosVentas[0][6]=4056;datosVentas[0][7]=3000;datosVentas[0][8]=2;datosVentas[0][9]=0;
		
		datosVentas[1][0]=4;datosVentas[1][1]=5;datosVentas[1][2]=2015;datosVentas[1][3]=8;datosVentas[1][4]=50;
		datosVentas[1][5]=1001;datosVentas[1][6]=3045;datosVentas[1][7]=2000;datosVentas[1][8]=4;datosVentas[1][9]=2;
		
		datosVentas[2][0]=25;datosVentas[2][1]=7;datosVentas[2][2]=2015;datosVentas[2][3]=17;datosVentas[2][4]=05;
		datosVentas[2][5]=1024;datosVentas[2][6]=4056;datosVentas[2][7]=3000;datosVentas[2][8]=5;datosVentas[2][9]=1;
		
		datosVentas[3][0]=2;datosVentas[3][1]=1;datosVentas[3][2]=2016;datosVentas[3][3]=11;datosVentas[3][4]=45;
		datosVentas[3][5]=1001;datosVentas[3][6]=4056;datosVentas[3][7]=3000;datosVentas[3][8]=3;datosVentas[3][9]=2;
		
		datosVentas[4][0]=10;datosVentas[4][1]=2;datosVentas[4][2]=2016;datosVentas[4][3]=10;datosVentas[4][4]=25;
		datosVentas[4][5]=1001;datosVentas[4][6]=3045;datosVentas[4][7]=2000;datosVentas[4][8]=1;datosVentas[4][9]=1;
		return datosVentas;
	}
}