
public class Registradur�a {

	public static void main(String[] args) {
		java.util.Scanner lectura = new java.util.Scanner(System.in);
		int municipios=0;
		int alcaldes=0;
		String nombresalcaldes[]=new String[alcaldes];
		String nombresmunicipios[]=new String[municipios];
		System.out.println("Bienvenido al programa.");
		System.out.println("El n�mero de municipios es:");
		while (true){
			municipios=lectura.nextInt();
			if  (municipios<=20){
				nombresmunicipios=new String [municipios+1];
				nombresmunicipios[0]=null;
				for (int i=1;i<municipios+1;i++){
					System.out.println("Ingrese el nombre del municipio "+i+":");
					nombresmunicipios[i]=lectura.next();
				}
				break;
			}else{
				System.out.println("El n�mero de municipios es mayor al permitido. Puede ingresar un n�mero m�ximo de 20.");// Informaci�n �til para el ususario.
				System.out.println("Vuelva a ingresar el n�mero de municipios:");
			}
		}
		System.out.println("El n�mero de alcades es:");
		while (true){
			alcaldes=lectura.nextInt();
			if (alcaldes<=20){
				nombresalcaldes=new String [alcaldes+1];
				nombresalcaldes[0]="|\t";
				for (int i=1;i<alcaldes+1;i++){
					System.out.println("Ingrese el nombre del alcalde "+i+":");
					nombresalcaldes[i]=lectura.next();
				}
				break;
			}else{
				System.out.println("El n�mero de alcaldes es mayor al permitido. Puede ingresar un n�mero m�ximo de 20.");// Informaci�n �til para el ususario.
				System.out.println("Vuelva a ingresar el n�mero de alcaldes:");
			}
		}	 
		int m[][]=new int[municipios+1][alcaldes+1];
		for(int i=0;i<municipios+1;i++){
			for(int j=0;j<alcaldes+1;j++){
				if (i==0){
					m[i][j]=0;
				}
				if (i>0 && j==0){
					m[i][j]=0;
				}
				if (i>0 && j>0){
					System.out.println("El n�mero de votos para el alcalde "+j+" en el municipio "+i+" es:");
					m[i][j]=lectura.nextInt();
				}	
			}
		}
		System.out.println("Tabla de votaciones:");
		Tabla(municipios,alcaldes,m,nombresalcaldes,nombresmunicipios);
		System.out.println(" ");
		System.out.println("N�meros de cada alcalde:");
		NumeroDeVotos(municipios,alcaldes,m,nombresalcaldes);
		System.out.println("Resultados:");
		AlcaldeMasVotado(municipios,alcaldes,m,nombresalcaldes);
		AnalisisDeVotos(municipios,alcaldes,m,nombresalcaldes);
		System.out.println("Informe final de votos:");
		InformeFinal(municipios,alcaldes,m,nombresalcaldes);
	}
	public static void Tabla(int municipios,int alcaldes,int m[][],String nombresalcaldes[],String nombresmunicipios[]){
		for(int i=0;i<municipios+1;i++){ // Condici�n necesaria para la elaboraci�n de la primera l�nea de la tabla. 
			for(int j=0;j<alcaldes+1;j++){
				if (i==0){
					System.out.print(nombresalcaldes[j]+"|");
				}
				if (i>0 && j==0){
					System.out.print("|"+nombresmunicipios[i]+"|");
				}
				if (i>0 && j>0){
					System.out.print("  "+m[i][j]+"  |");
				}
			}
			System.out.println(" ");
		}
	}
	public static void NumeroDeVotos(int municipios,int alcaldes,int m[][],String nombresalcaldes[]){
		double g=0; // Variable que recibe el valor de la suma.
		for(int i=1;i<municipios+1;i++){ //Ciclo para hallar la suma total de votos.
			for(int j=1;j<alcaldes+1;j++){
				g+=m[i][j];
			}
		}	
		for(int j=1;j<alcaldes+1;j++){ // Ciclo que saca la suma total de votos de todos los alcade.
			System.out.print(nombresalcaldes[j]);
			double d=0;// La variable d ayuda a separar las sumas de votos de cada alcalde.
			for(int i=1;i<municipios+1;i++){ //Ciclo que saca la suma total de votos de cada alcalde.
				d+=m[i][j];
			}
			System.out.print(". N�mero total de votos: "+d);
			double h=((d/g)*100); // h es la variable del porcentaje.
			System.out.println(". Porcentaje de votos: "+h+"%");
		}
	}
	public static void AlcaldeMasVotado(int alcaldes,int municipios,int m[][],String nombresalcaldes[]){
		int n=0;
		int o=0;
		for(int j=1;j<alcaldes+1;j++){
			int d=0;
			for(int i=1;i<municipios+1;i++){
				d+=m[i][j];
			}
			if (d>n){
				n=d; // n es una variable que permite guardar el valor temporalde la suma mayor.
				o=j; // o es una variable que me permite guardar el n�mero del alcalde.
			}
		}
		System.out.println(nombresalcaldes[o]+" es el alcalde m�s votado.");
	}
	public static void AnalisisDeVotos(int municipios,int alcaldes,int m[][],String nombresalcaldes[]){
		double g=0;
		for(int i=1;i<municipios+1;i++){
			for(int j=1;j<alcaldes+1;j++){
				g+=m[i][j];
			}
		}
		double q[]=new double [alcaldes]; // array que permite guardar el porcentaje de votos de todos los alcaldes.
		for (int i=0;i<alcaldes;i++){
			q[i]=0;
		}
		int s[]=new int [alcaldes]; // array que permite guardar el n�mero de cada alcalde. 
		for (int i=0;i<alcaldes;i++){
			s[i]=0;
		}
		for(int j=1;j<alcaldes+1;j++){
			double d=0;
			for(int i=1;i<municipios+1;i++){
				d+=m[i][j];
			}
			double h=((d/g)*100);
			if (h>50){ // Condici�n que permite saber si hay un gandor de las votaciones.
				System.out.println(nombresalcaldes[j]+" es el alcalde ganador de las votaciones.");
			}else{
				for (int k=0;k<alcaldes;k++){ // Ciclo que permite organizar los porcentajes de mayor a menor dentro del array.
					if (h>q[k]){
						if (k<alcaldes-1){ // Condici�n que permite cambiar el orden de los porcentajes sin que exceda la capacidad del array.  
							q[k+1]=q[k];
							s[k+1]=s[k]; 
						}
						q[k]=h;
						s[k]=j; // Funci�n que permite seguir el orden de los porcentajes con el n�mero de los pel�culas. 
						break; // El break permite buscar el lugar del porcentaje en cuesti�n sin que afecte los dem�s puestos.
					}
				}
			}
		}
		double r=0;
		for (int i=0;i<alcaldes;i++){
			r+=q[i];
			if (r>50){ // Cuando los porcentajes mayoes suman m�s de 50 se nombran los alcaldes que partician en esa suma.
				for (int k=0;k<i;k++){
					System.out.println(nombresalcaldes[(s[k])]+" pasa a la segunda ronda de votaciones.");
				}
				break;
			}
		}
	}
	public static void InformeFinal(int municipios,int alcaldes,int m[][],String nombresalcaldes[]){
		int t[]=new int [alcaldes];
		for (int i=0;i<alcaldes;i++){
			t[i]=0;
		}
		int v[]=new int [alcaldes];
		for (int i=0;i<alcaldes;i++){
			v[i]=0;
		}
		for(int j=1;j<alcaldes+1;j++){
			int d=0;
			for(int i=1;i<municipios+1;i++){
				d+=m[i][j];
				for (int k=0;k<alcaldes;k++){ // Ciclo parecido al de la funci�n anterior, con la diferencia de que en vez de porcentajes maneja n�mero de votos por alcalde. 
					if (d>v[k]){
						if (k<alcaldes-1){
							v[k+1]=v[k];
							t[k+1]=t[k];
						}
						v[k]=d;
						t[k]=j;
						break;
					}
				}
			}
		}
		for (int i=0;i<alcaldes;i++){
			System.out.println(nombresalcaldes[t[i]]+": "+v[i]+" votos.");
		}
	}
}