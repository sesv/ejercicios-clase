import java.util.*;
public class Familia {
	java.util.Scanner lectura=new java.util.Scanner(System.in);
    ArrayList<Integrante> integrantes= new ArrayList<>();

    public Familia() {
        for (int i=0;i<4;i++){
            System.out.println("Ingrese el nombre del integrante: ");
            String x=lectura.next();
            System.out.println("Ingrese el apellido del integrante: ");
            String y=lectura.next();
            System.out.println("Ingrese la edad del integrante: ");
            int z=lectura.nextInt();
            System.out.println("Ingrese el sexo del integrante: ");
            String w=lectura.next();
            System.out.println("Ingrese el rol del integrante: ");
            String v=lectura.next();
            Integrante a= new Integrante(x,y,z,w,v);
            integrantes.add(a);
        }
    }

    public ArrayList<Integrante> getIntegrantes() {
        return integrantes;
    }
    
}
